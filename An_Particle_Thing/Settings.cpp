#include "Settings.h"



Settings::Settings()
{
}


Settings::~Settings()
{
}


bool Settings::loadFromFile(std::string filename)
{
	// open the file
	std::ifstream inputFileStream;
	inputFileStream.open(filename);

	// check for errors, even though there shouldnt be any
	if (!inputFileStream)
	{
		std::cout << "Error opening file to read. How did this happen?\n";
		return false;
	}

	// stores the line read from the settings file
	std::string line = "";

	// reads from file while there is something to read
	while(inputFileStream >> line)
	{
		// overrides settings based on file contents
		if (line == "XLimit")
		{
			inputFileStream >> this->XLimit;
		}
		else if (line == "YLimit")
		{
			inputFileStream >> this->YLimit;
		}
		else if (line == "borderCollisionSpeedPenalty")
		{
			inputFileStream >> this->borderCollisionSpeedPenalty;
		}
		else if (line == "borderMode")
		{
			inputFileStream >> this->borderMode;
		}
		else if (line == "wrapSpeedPenaltyEnabled")
		{
			inputFileStream >> this->wrapSpeedPenaltyEnabled;
		}
		else if (line == "attractionBorderWrapEnabled")
		{
			inputFileStream >> this->attractionBorderWrapEnabled;
		}


		else if (line == "physicsEnabled")
		{
			inputFileStream >> this->physicsEnabled;
		}
		else if (line == "attractionPointMass")
		{
			inputFileStream >> this->attractionPointMass;
		}
		else if (line == "timeMultiplier")
		{
			inputFileStream >> this->timeMultiplier;
		}
		else if (line == "attractionDistanceExponent")
		{
			inputFileStream >> this->attractionDistanceExponent;
		}
		else if (line == "minimumAttractionDistance")
		{
			inputFileStream >> this->minimumAttractionDistance;
		}
		else if (line == "maximalPhysicsTimePassed")
		{
			inputFileStream >> this->maximalPhysicsTimePassed;
		}
		else if (line == "movementDampeningCoefficient")
		{
			inputFileStream >> this->movementDampeningCoefficient;
		}
		else if (line == "unrealisticDampeningEnabled")
		{
			inputFileStream >> this->unrealisticDampeningEnabled;
		}

		if (line == "pointsOrLines")
		{
			inputFileStream >> this->pointsOrLines;
		}

		else if (line == "nextColor_red")
		{
			inputFileStream >> this->nextColor_red;
		}
		else if (line == "nextColor_green")
		{
			inputFileStream >> this->nextColor_green;
		}
		else if (line == "nextColor_blue")
		{
			inputFileStream >> this->nextColor_blue;
		}


		else if (line == "background_red")
		{
			inputFileStream >> this->background_red;
		}
		else if (line == "background_green")
		{
			inputFileStream >> this->background_green;
		}
		else if (line == "background_blue")
		{
			inputFileStream >> this->background_blue;
		}
	}

	// closes file stream
	inputFileStream.close();

	return true;
}


bool Settings::saveToFile(std::string filename)
{
	// opens the file
	std::ofstream outputFileStream;
	outputFileStream.open(filename);

	// checks for errors, even though there shouldnt be any
	if (!outputFileStream)
	{
		std::cout << "Error opening file to write. How did this happen?\n";
		return false;
	}

	// writes all settings to the file

	outputFileStream << "XLimit\n";
	outputFileStream << this->XLimit << "\n";
	outputFileStream << "YLimit\n";
	outputFileStream << this->YLimit << "\n";
	outputFileStream << "borderCollisionSpeedPenalty\n";
	outputFileStream << this->borderCollisionSpeedPenalty << "\n";
	outputFileStream << "borderMode\n";
	outputFileStream << this->borderMode << "\n";
	outputFileStream << "wrapSpeedPenaltyEnabled\n";
	outputFileStream << this->wrapSpeedPenaltyEnabled << "\n";
	outputFileStream << "attractionBorderWrapEnabled\n";
	outputFileStream << this->attractionBorderWrapEnabled << "\n";

	outputFileStream << "physicsEnabled\n";
	outputFileStream << this->physicsEnabled << "\n";
	outputFileStream << "attractionPointMass\n";
	outputFileStream << this->attractionPointMass << "\n";
	outputFileStream << "timeMultiplier\n";
	outputFileStream << this->timeMultiplier << "\n";
	outputFileStream << "attractionDistanceExponent\n";
	outputFileStream << this->attractionDistanceExponent << "\n";
	outputFileStream << "minimumAttractionDistance\n";
	outputFileStream << this->minimumAttractionDistance << "\n";
	outputFileStream << "maximalPhysicsTimePassed\n";
	outputFileStream << this->maximalPhysicsTimePassed << "\n";
	outputFileStream << "movementDampeningCoefficient\n";
	outputFileStream << this->movementDampeningCoefficient << "\n";
	outputFileStream << "unrealisticDampeningEnabled\n";
	outputFileStream << this->unrealisticDampeningEnabled << "\n";

	outputFileStream << "pointsOrLines\n";
	outputFileStream << this->pointsOrLines << "\n";

	outputFileStream << "nextColor_red\n";
	outputFileStream << this->nextColor_red << "\n";
	outputFileStream << "nextColor_green\n";
	outputFileStream << this->nextColor_green << "\n";
	outputFileStream << "nextColor_blue\n";
	outputFileStream << this->nextColor_blue << "\n";

	outputFileStream << "background_red\n";
	outputFileStream << this->background_red << "\n";
	outputFileStream << "background_green\n";
	outputFileStream << this->background_green << "\n";
	outputFileStream << "background_blue\n";
	outputFileStream << this->background_blue << "\n";

	// close the file stream
	outputFileStream.close();

	return true;
}

#include "Particle.h"

Particle::Particle(sf::Vector2f spawnPosition, sf::Color color, sf::Vertex* vertices)
{
	this->position = spawnPosition;

	for (int i = 0; i < 4; ++i)
	{
		vertices[i].color = color;
	}

	vertices[0].position = spawnPosition + sf::Vector2f(-offset, -offset);
	vertices[1].position = spawnPosition + sf::Vector2f(offset, -offset);
	vertices[2].position = spawnPosition + sf::Vector2f(offset, offset);
	vertices[3].position = spawnPosition + sf::Vector2f(-offset, offset);
}

void Particle::setPosition(sf::Vector2f pos)
{
	this->position = pos;
}

void Particle::setX(float x)
{
	this->position.x = x;
}

void Particle::setY(float y)
{
	this->position.y = y;
}

sf::Vector2f Particle::getPosition()
{
	return this->position;
}

void Particle::move(float timePassed, sf::Vertex* vertices)
{
	this->position += this->velocity * timePassed;

	vertices[0].position.x = this->position.x - offset;
	vertices[0].position.y = this->position.y - offset;

	vertices[1].position.x = this->position.x + offset;
	vertices[1].position.y = this->position.y - offset;

	vertices[2].position.x = this->position.x + offset;
	vertices[2].position.y = this->position.y + offset;

	vertices[3].position.x = this->position.x - offset;
	vertices[3].position.y = this->position.y + offset;
}

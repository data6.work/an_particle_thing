#pragma once

#include <vector>
#include <cmath>

#include "SFML/Graphics.hpp"

#include "Settings.h"
#include "Particle.h"

// Processes particle movement and attraction, draws the particles
class ParticleManager
{
public:
	ParticleManager();

	// spawns particle at passed coordinates
	void spawnParticle(sf::Vector2f position, bool spawnmany);

	// processes particle movement, acceleration
	// and draws them onto the passed window object
	void processAndDrawParticles(sf::RenderWindow* window,
								 sf::Vector2f attractionPosition,
								 bool attractionEnabled,
								 float timePassed);

	void removeAllParticles();

	int getParticleAmount();

	// sets all particles velocity to 0
	void haltAllMovement();

	// stops and places each particle in a specific order
	void orderParticles();

	// returns current simulation settings pointer
	Settings* getSettings();

private:
	void createNewParticle(sf::Vector2f position, sf::Color color);

	// applies attraction to particle velocity
	void processAttraction(Particle* particle,
						   float timePassed,
						   sf::Vector2f attractionPointPosition,
						   bool attractionEnabled);

	// Process attraction wrapping through the border.
	// Propperly alters passed X and Y differences
	void processAttractionWrap(float* XDifference,
							   float* YDifference);


	// applies movement dampening to the passed velocity
	void processFriction(sf::Vector2f* velocity,
						 float timePassed);


	// Processes border collisions depending on collision mode.
	// Returns true if the current particle was deleted by the void
	void processBorderCollisions(Particle* particle,
								 int* deletionCounter);

	///// Functions for specific kinds of border collisions /////
	//
	// ckecks wrapped border collisions
	void checkWrapBorderCollisons(Particle* particle);
	//
	// ckecks reflective border collisions
	void checkReflectiveBorderCollisions(Particle* particle);
	//
	// ckecks void border collisions
	bool isOutOfBorder(sf::Vector2f position);
	//
	/////////////////////////////////////////////////////////////


	// draws particles onto window
	void drawParticles(sf::RenderWindow* window);

	// returns physics time passed
	float getPhysicsTimePassed(float actualTimePassed);

	// returns true if there is a baarticle in passed coordiantes
	bool isAParticleOnThisPosition(sf::Vector2f position);

	// removes particle at the specified index
	void removeParticle(int index);

	// stores simulation settings
	Settings settings;

	// stores particles
	std::vector<Particle> particles;
	std::vector<sf::Vertex> bodies;
	const static int reserveSize = 100000;
};

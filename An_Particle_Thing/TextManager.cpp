#include "TextManager.h"

/////////////////////////////////////////////////////
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //
// WARNING: SPAGHETTI CODE WHEN WORKING WITH TEXTS //
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //
/////////////////////////////////////////////////////

TextManager::TextManager()
{
	// loads the font on creation, throws exception on fail
	if (!this->textFont.loadFromFile(".\\Res\\TextFont.ttf"))
	{
		throw -1;
	}

	// sets up texts
	this->initializeTexts();
	this->setupTextsPositions();
	this->setUpStaticStrings();
}


void TextManager::setupTextsPositions()
{
	// coordinates step allows changing coordinates automaticly
	// after placing the text
	const float YStep = this->characterSize * 1.3f;
	float YOffset = -YStep;
	float XOffset = 0.f;

	// upper left corner text
	for (int index = TextNames::upperLeftFirstID;
		 index <= TextNames::upperLeftLastID;
		 ++index)
	{
		this->texts[index].setPosition(XOffset, YOffset += YStep);
	}

	// lower left text
	YOffset = 730.f;
	for (int index = TextNames::lowerLeftFirstID;
		 index <= TextNames::lowerLeftLastID;
		 ++index)
	{
		this->texts[index].setPosition(XOffset, YOffset += YStep);
	}

	// lower right text
	YOffset = 885.f;
	XOffset = 1475.f;
	for (int index = TextNames::lowerRightFirstID;
		 index <= TextNames::lowerRightLastID;
		 ++index)
	{
		this->texts[index].setPosition(XOffset, YOffset += YStep);
	}

	// upper right texts
	YOffset = -YStep;
	for (int index = TextNames::upperRightFirstID;
		 index <= TextNames::upperRightLastID;
		 ++index)
	{
		this->texts[index].setPosition(XOffset, YOffset += YStep);
	}

	// control description text
	YOffset = 200.f;
	XOffset = 0.f;
	for (int index = TextNames::controlDescriptionFirstID;
		 index <= TextNames::controlDescriptionLastID;
		 ++index)
	{
		this->texts[index].setPosition(XOffset, YOffset += YStep);
	}
}


void TextManager::setUpStaticStrings()
{
	// sets up the strings of the unchangeable texts

	this->texts[TextNames::controlDescriptionPrompt].setString(
		"Press I to see controls");

	this->texts[TextNames::controlDescription_0].setString(
		"Left mouse button to create particles, right one to spawn them, hold Shift to spawn more");
	this->texts[TextNames::controlDescription_1].setString(
		"Q/E to control attraction point mass, W to reverse it");
	this->texts[TextNames::controlDescription_2].setString(
		"A/Z to control time multiplier, S/X to control attraction distance exponent");
	this->texts[TextNames::controlDescription_3].setString(
		"D/C to control minimum attraction distance, F/V to control maximal passed physics time");
	this->texts[TextNames::controlDescription_4].setString(
		"G/B to control movement dampening coefficient, H to toggle unrealistic dampening");
	this->texts[TextNames::controlDescription_5].setString(
		"J to toggle between wrapping, reflective and void borders. N to toggle speed penalty on border wrap, M to toggle attraction wrapping borders, K/L to cotrol border speed penalty");
	this->texts[TextNames::controlDescription_6].setString(
		"R to erase all particles, T to stop them, Y to order particles in a line");
	this->texts[TextNames::controlDescription_7].setString(
		"U to disable frame clearing, I to show controls, O to hide all text, P to make a screenshot, Ctrl to save/load settings from file");
	this->texts[TextNames::controlDescription_8].setString(
		"Hold Shift to make more precise changes, numbers 0-9 to change the color of the next particle, Tab to change drawing mode, Escape to exit");
}


void TextManager::updateAndDrawTexts(sf::RenderWindow* renderTarget,
									 Settings* settings,
									 int particleAmount,
									 float timePasedSeconds)
{
	// updates texts which require passed parameters
	this->updateTextsUnrelatedToSettings(particleAmount, timePasedSeconds);

	int textAmount = this->texts.size();
	for (int index = 0; index < textAmount; ++index)
	{
		// updates the texts which show the current settings
		this->updateSettingsTexts(settings, &this->texts[index]);

		// draws the text if it is visible
		if (this->texts[index].isVisible)
		{
			renderTarget->draw(this->texts[index]);
		}
	}
}


void TextManager::updateSettingsTexts(Settings* settings,
									  Text* updateTarget)
{
	// updates string of the update target text depending on the
	// text name
	switch (updateTarget->name)
	{
		// lower left text names

	case TextNames::physicsEnabled:
		if (settings->physicsEnabled)
		{
			updateTarget->setString("Physics enabled: true");
		}
		else
		{
			updateTarget->setString("Physics enabled: false");
		}
		break;

	case TextNames::attractionPointMass:
		updateTarget->setString("Attraction point mass: " +
			std::to_string(settings->attractionPointMass / 100000.f));
		break;

	case TextNames::minimumAttractionDistance:
		updateTarget->setString("Minimum attraction distance: " +
			std::to_string(settings->minimumAttractionDistance));
		break;

	case TextNames::attractionDistanceExponent:
		updateTarget->setString("Attraction distance exponent: " +
			std::to_string(settings->attractionDistanceExponent));
		break;

	case TextNames::timeMultiplier:
		updateTarget->setString("Time multiplier: " +
			std::to_string(settings->timeMultiplier));
		break;

	case TextNames::maximalPhysicsTimePassed:
		updateTarget->setString("Maximal physics time passed: " +
			std::to_string(settings->maximalPhysicsTimePassed));
		if (settings->maximalPhysicsTimeOverdue)
		{
			updateTarget->setFillColor(sf::Color::Yellow);
		}
		else
		{
			updateTarget->setFillColor(sf::Color::White);
		}
		break;

	case TextNames::movementDampeningCoefficient:
		updateTarget->setString("Movement dampening coefficient: " +
			std::to_string(settings->movementDampeningCoefficient));
		break;

	case TextNames::unrealisticDampeningEnabled:
		if (settings->unrealisticDampeningEnabled)
		{
			updateTarget->setString("Unrealistic dampening enabled: true");
		}
		else
		{
			updateTarget->setString("Unrealistic dampening enabled: false");
		}
		break;

		// lower right text names

	case TextNames::borderCollisionSpeedPenalty:
		updateTarget->setString("Border collision speed penalty: " +
			std::to_string(settings->borderCollisionSpeedPenalty));
		break;

	case TextNames::wrapBordersEnabled:
		switch (settings->borderMode)
		{
		case 0:
			updateTarget->setString("Border mode: wrapping");
			break;

		case 1:
			updateTarget->setString("Border mode: reflective");
			break;

		case 2:
			updateTarget->setString("Border mode: void");
		}
		break;

	case TextNames::wrapSpeedPenaltyEnabled:
		if (settings->wrapSpeedPenaltyEnabled)
		{
			updateTarget->setString("Wrap speed penalty enabled: true");
		}
		else
		{
			updateTarget->setString("Wrap speed penalty enabled: false");
		}
		break;

	case TextNames::attractionBorderWrapEnabled:
		if (settings->attractionBorderWrapEnabled)
		{
			updateTarget->setString("Attraction wraps borders: true");
		}
		else
		{
			updateTarget->setString("Attraction wraps borders: false");
		}
		break;
	}
}


void TextManager::updateTextsUnrelatedToSettings(int particleAmount,
												 float timePassedSeconds)
{
	// updates particle amount text
	this->texts[TextNames::particleCounter].setString(
		"Particle amount: " + std::to_string(particleAmount));

	// updates fps text
	this->texts[TextNames::fpsCounter].setString(
		"FPS: " + std::to_string(int(1.f / timePassedSeconds)));
}


void TextManager::initializeTexts()
{
	// i died 4 times while making this, but at least now its < 100 lines

	for (int index = TextNames::firstTextID;
		 index < TextNames::textAmount;
		 ++index)
	{
		// create text with index as its name and set it up
		this->texts.push_back(Text(TextNames(index)));
		this->applyTextSettings(&this->texts.back());
	}

	// hides control description texts
	for (int index = TextNames::controlDescriptionFirstID;
		 index <= TextNames::controlDescriptionLastID;
		 ++index)
	{
		this->texts[index].isVisible = false;
	}
}


void TextManager::applyTextSettings(Text* targetText)
{
	// TODO: remove this, this exists only for debugging
	if (targetText == nullptr)
	{
		throw -12;
	}

	targetText->setFont(this->textFont);

	targetText->setCharacterSize(this->characterSize);
	targetText->setOutlineThickness(this->characterSize *
		this->outlineSizeToCharacterSize);

	targetText->setFillColor(this->bodyColor);
	targetText->setOutlineColor(this->outlineColor);
}

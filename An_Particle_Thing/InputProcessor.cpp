#include "InputProcessor.h"



InputProcessor::InputProcessor(sf::RenderWindow* window,
							   ParticleManager* particleManager,
							   TextManager* textManager):
							   window(window),
							   particleManager(particleManager),
							   textManager(textManager)
{
	// saves window resolution
	this->windowResolution = this->window->getDefaultView().getSize();
}


void InputProcessor::processInput()
{
	// process all pending events
	sf::Event event;
	while (this->window->pollEvent(event))
	{
		// calls a function depending on the event type
		switch (event.type)
		{
		case sf::Event::Closed:
			this->processClosedEvent();
			break;

		case sf::Event::KeyPressed:
			this->processKeyboardEvents();
			break;
		}
	}

	// processes mouse button state
	this->processMouseButton();
}


bool InputProcessor::isLMBPressed()
{
	return sf::Mouse::isButtonPressed(sf::Mouse::Button::Left);
}


bool InputProcessor::isRMBPressed()
{
	return sf::Mouse::isButtonPressed(sf::Mouse::Button::Right);
}


sf::Vector2f InputProcessor::getMousePosition()
{
	sf::Vector2f relativeToWindow = (sf::Vector2f)sf::Mouse::getPosition();
	return this->translateWindowCoordinatesToGameCoordinates(relativeToWindow);
}


void InputProcessor::processClosedEvent()
{
	this->window->close();
	this->userRequestedExit = true;
}


void InputProcessor::processKeyboardEvents()
{
	// gets current settings reference
	Settings* currentSettings = this->particleManager->getSettings();

	// Checks for keys pressed and perform appropriate action.

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Space))
	{
		// Toggles physicsEnabled state of current settings.
		// Sets it to false if enabled, true if disabled
		currentSettings->physicsEnabled =
			!currentSettings->physicsEnabled;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::E))
	{
		// increases attraction point mass
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
		{
			currentSettings->attractionPointMass += 10000.0f;
		}
		else
		{
			currentSettings->attractionPointMass += 100000.0f;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Q))
	{
		// decreases attraction point mass
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
		{
			currentSettings->attractionPointMass -= 10000.0f;
		}
		else
		{
			currentSettings->attractionPointMass -= 100000.0f;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::W))
	{
		// reserves attraction point mass
		currentSettings->attractionPointMass *= -1.0f;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::R))
	{
		// erases all particles
		this->particleManager->removeAllParticles();
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::T))
	{
		// stops all particles
		this->particleManager->haltAllMovement();
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Y))
	{
		// orders all particles
		this->particleManager->orderParticles();
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::A))
	{
		// increases time multiplier
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
		{
			currentSettings->timeMultiplier += 0.01f;
		}
		else
		{
			currentSettings->timeMultiplier += 0.1f;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Z))
	{
		// decreases time multiplier
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
		{
			currentSettings->timeMultiplier -= 0.01f;
		}
		else
		{
			currentSettings->timeMultiplier -= 0.1f;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::S))
	{
		// increases attractionDistanceExponent
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
		{
			currentSettings->attractionDistanceExponent += 0.01f;
		}
		else
		{
			currentSettings->attractionDistanceExponent += 0.1f;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::X))
	{
		// decreases attractionDistanceExponent
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
		{
			currentSettings->attractionDistanceExponent -= 0.01f;
		}
		else
		{
			currentSettings->attractionDistanceExponent -= 0.1f;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::D))
	{
		// increases minimumAttractionDistance
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
		{
			currentSettings->minimumAttractionDistance += 0.01f;
		}
		else
		{
			currentSettings->minimumAttractionDistance += 0.1f;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::C))
	{
		// decreases minimumAttractionDistance
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
		{
			currentSettings->minimumAttractionDistance -= 0.01f;
		}
		else
		{
			currentSettings->minimumAttractionDistance -= 0.1f;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::F))
	{
		// increases maximalPhysicsTimePassed
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
		{
			currentSettings->maximalPhysicsTimePassed += 1E-4f;
		}
		else
		{
			currentSettings->maximalPhysicsTimePassed += 1E-3f;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::V))
	{
		// decreases maximalPhysicsTimePassed
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
		{
			currentSettings->maximalPhysicsTimePassed -= 1E-4f;
		}
		else
		{
			currentSettings->maximalPhysicsTimePassed -= 1E-3f;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::G))
	{
		// increases movementDampeningCoefficient
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
		{
			currentSettings->movementDampeningCoefficient += 0.01f;
		}
		else
		{
			currentSettings->movementDampeningCoefficient += 0.1f;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::B))
	{
		// decreases movementDampeningCoefficient
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
		{
			currentSettings->movementDampeningCoefficient -= 0.01f;
		}
		else
		{
			currentSettings->movementDampeningCoefficient -= 0.1f;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::H))
	{
		// toggles experimental movement dampening
		currentSettings->unrealisticDampeningEnabled =
			!currentSettings->unrealisticDampeningEnabled;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::J))
	{
		// Switches throught border mode.
		// wrap -> reflection -> void
		this->switchBorderMode(currentSettings);
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::N))
	{
		// toggles wrap borders speed penalty
		currentSettings->wrapSpeedPenaltyEnabled =
			!currentSettings->wrapSpeedPenaltyEnabled;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::M))
	{
		// toggles attraction wrapping through border
		currentSettings->attractionBorderWrapEnabled =
			!currentSettings->attractionBorderWrapEnabled;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::L))
	{
		// increases borderCollisionSpeedPenalty
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
		{
			currentSettings->borderCollisionSpeedPenalty += 0.01f;
		}
		else
		{
			currentSettings->borderCollisionSpeedPenalty += 0.1f;
		}
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::K))
	{
		// decreases borderCollisionSpeedPenalty
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift))
		{
			currentSettings->borderCollisionSpeedPenalty -= 0.01f;
		}
		else
		{
			currentSettings->borderCollisionSpeedPenalty -= 0.1f;
		}
	}


	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::U))
	{
		// toggles frame clearing
		currentSettings->frameClearingEnabled =
			!currentSettings->frameClearingEnabled;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::I))
	{
		// toggles controls description
		this->toggleControlTextDescription();
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::O))
	{
		// toggle the visibility state of all texts until
		// control description text
		this->toggleMainTextVisibility();
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::P))
	{
		// saves screenshot
		this->saveScreenshot();
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LControl))
	{
		// prompts user to saave/load settings
		this->requestSettingsReadWrite();
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Escape))
	{
		// leaves simulation when pressed Esc
		this->processClosedEvent();
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Tab))
	{
		currentSettings->pointsOrLines = !currentSettings->pointsOrLines;
	}

	// checks number keys 0 to 9 to apply colors accordingly

	// firstly tries numpad keys
	for (int keyId = sf::Keyboard::Key::Numpad0;
		keyId <= sf::Keyboard::Key::Numpad9;
		++keyId)
	{
		// call color assigner if found a match
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(keyId)))
		{
			// use key id of the first ckecked key as color key
			this->setNextColor(keyId - sf::Keyboard::Numpad0);
		}
	}

	// tries not-numpad number keys
	for (int keyId = sf::Keyboard::Key::Num0;
		keyId <= sf::Keyboard::Key::Num9;
		++keyId)
	{
		// call color assigner if found a match
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key(keyId)))
		{
			// use key of the first ckecked key as color key
			this->setNextColor(keyId - sf::Keyboard::Num0);
		}
	}
}


void InputProcessor::processMouseButton()
{
	// spawn particle if left mouse button is pressed
	if (this->isLMBPressed())
	{
		this->spawnParticle();
	}
}


void InputProcessor::saveScreenshot()
{
	sf::Image screenshot = this->window->capture();

	std::string filename = "screenshot-" +
						   std::to_string(time(0)) +
						   ".png";
	if (!screenshot.saveToFile(filename))
	{
		throw -10;
	}
}


void InputProcessor::toggleMainTextVisibility()
{
	// toggle the state of all main texts
	for (int index = TextNames::mainTextFirstID;
		index <= TextNames::mainTextLastID;
		++index)
	{
		this->textManager->texts[index].isVisible =
			!this->textManager->texts[index].isVisible;
	}
}


void InputProcessor::toggleControlTextDescription()
{
	// toggles the visibility state of control description texts
	for (int index = TextNames::controlDescriptionFirstID;
		index <= TextNames::controlDescriptionLastID;
		++index)
	{
		this->textManager->texts[index].isVisible =
			!this->textManager->texts[index].isVisible;
	}
}


void InputProcessor::setNextColor(int colorKey)
{
	// get settings reference where colors will be edited
	Settings* settings = this->particleManager->getSettings();

	// assigns a color to the next particle based on the color key
	switch (colorKey)
	{
	case 0:
		// white
		settings->nextColor_red = 255;
		settings->nextColor_green = 255;
		settings->nextColor_blue = 255;
		break;
		
	case 1:
		// violet
		settings->nextColor_red = 138;
		settings->nextColor_green = 43;
		settings->nextColor_blue = 255;
		break;

	case 2:
		// blue
		settings->nextColor_red = 0;
		settings->nextColor_green = 0;
		settings->nextColor_blue = 255;
		break;

	case 3:
		// i have no clue how to name this color
		settings->nextColor_red = 45;
		settings->nextColor_green = 100;
		settings->nextColor_blue = 245;
		break;

	case 4:
		// green-blueish
		settings->nextColor_red = 0;
		settings->nextColor_green = 255;
		settings->nextColor_blue = 255;
		break;

	case 5:
		// green
		settings->nextColor_red = 0;
		settings->nextColor_green = 255;
		settings->nextColor_blue = 0;
		break;

	case 6:
		// yellow
		settings->nextColor_red = 255;
		settings->nextColor_green = 255;
		settings->nextColor_blue = 0;
		break;

	case 7:
		// orange
		settings->nextColor_red = 255;
		settings->nextColor_green = 150;
		settings->nextColor_blue = 0;
		break;

	case 8:
		// red
		settings->nextColor_red = 255;
		settings->nextColor_green = 0;
		settings->nextColor_blue = 0;
		break;

	case 9:
		// black
		settings->nextColor_red = 0;
		settings->nextColor_green = 0;
		settings->nextColor_blue = 0;
		break;
	}
}


void InputProcessor::requestSettingsReadWrite()
{
	// close the window for the duration of the operation
	this->window->close();

	// asks user to chose whether to read or write settings
	FileSelector::requestSettingsReadWrite(
		this->particleManager->getSettings());

	// re-open the window
	this->window->create(sf::VideoMode::getDesktopMode(),
						 "An_Particle_Thing",
						 sf::Style::Fullscreen);
}


void InputProcessor::spawnParticle()
{
	// spawns particle, spawn many if shift is pressed
	this->particleManager->spawnParticle(
		this->getMousePosition(),
		sf::Keyboard::isKeyPressed(sf::Keyboard::Key::LShift));
}


void InputProcessor::switchBorderMode(Settings* currentSettings)
{
	// Switches throught border mode.
	// wrap -> reflection -> void
	switch (currentSettings->borderMode)
	{
	case 0:
		currentSettings->borderMode = 1;
		break;

	case 1:
		currentSettings->borderMode = 2;
		break;

	case 2:
		currentSettings->borderMode = 0;
	}
}


sf::Vector2f InputProcessor::translateWindowCoordinatesToGameCoordinates(
	sf::Vector2f windowCoordinates)
{
	sf::Vector2f translatedCoordinates = { 0.0f, 0.0f };

	Settings* settings = this->particleManager->getSettings();

	// converts coordinates relative to window to coordinates
	// relative to the simulation size, stored in settings
	translatedCoordinates.x = windowCoordinates.x * (settings->XLimit /
													 this->windowResolution.x);
	translatedCoordinates.y = windowCoordinates.y * (settings->YLimit /
													 this->windowResolution.y);

	return translatedCoordinates;
}

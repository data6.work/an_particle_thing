#pragma once

#include <string>
#include <fstream>
#include <iostream>

// stores world settings
class Settings
{
public:
	Settings();
	~Settings();

	// Loads settings from file.
	// Returns true on success, false on failure.
	bool loadFromFile(std::string filename);

	// Saves settings to file.
	// Returns true on success, false on failure.
	bool saveToFile(std::string filename);

	// world borders settings
	float XLimit = 1920.0f;
	float YLimit = 1080.0f;
	float borderCollisionSpeedPenalty = 0.5f;
	int borderMode = 1; // 0 - wrap, 1 - reflection, 2 - void
	bool wrapSpeedPenaltyEnabled = false;
	bool attractionBorderWrapEnabled = false;

	// physics settings
	bool physicsEnabled = true;
	float attractionPointMass = 4E6f;
	float timeMultiplier = 1.0f;
	float attractionDistanceExponent = 2.f;
	float minimumAttractionDistance = 10.0f;
	float maximalPhysicsTimePassed = 15E-3f;
	float movementDampeningCoefficient = 0.f;
	bool unrealisticDampeningEnabled = false;

	// Stores the color of the next spawning particle.
	// Color format - RGB
	int nextColor_red = 138;
	int nextColor_green = 43;
	int nextColor_blue = 255;

	// stores background colors
	int background_red = 50;
	int background_green = 50;
	int background_blue = 50;

	// false - draw points, true - draw lines
	bool pointsOrLines = false;

	//
	// temporary varibles, not intended for saving
	//

	// indicates if frame clearing is enabled
	bool frameClearingEnabled = true;

	// it is set to true or false by ParticleManager, depending on
	// overdue of the maximal physics time
	bool maximalPhysicsTimeOverdue = false;
};

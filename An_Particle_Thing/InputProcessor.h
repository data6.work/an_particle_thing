#pragma once

#include <string>
#include <chrono>
#include <thread>

#include "SFML/Graphics.hpp"

#include "TextManager.h"
#include "ParticleManager.h"
#include "FileSelector.h"
#include "Settings.h"

// Processes graphical window events and user keyboard input.
// Must be initialized with graphical window, particle
// manager and a text manager pointers
class InputProcessor
{
public:
	InputProcessor(sf::RenderWindow* window,
				   ParticleManager* particleManager,
				   TextManager* textManager);

	// checks inputs and events and calls functions to process them
	void processInput();

	// returns true if the coresponding mouse button is pressed
	bool isLMBPressed();
	bool isRMBPressed();

	// returns mouse position relative to simulation
	sf::Vector2f getMousePosition();

private:
	// processes event where user has closed the window
	void processClosedEvent();

	// processes keyboard events
	void processKeyboardEvents();

	// processes mouse button presses
	void processMouseButton();

	///// functions for performing specific actions requested by user /////
	//
	// saves screenshot of the window on user request
	void saveScreenshot();
	//
	// toggles main text visibility
	void toggleMainTextVisibility();
	//
	// toggles visibility of control description text
	void toggleControlTextDescription();
	//
	// sets future particle color
	void setNextColor(int colorKey);
	//
	// prompts user to save/load settigns
	void requestSettingsReadWrite();
	//
	// spawns particles at mouse position
	void spawnParticle();
	//
	// switches through the border mode
	void switchBorderMode(Settings* currentSettings);
	//
	//////////////////////////////////////////////////////////////

	// transforms coordinates relative to the window to the
	// coordinates relative to the game
	sf::Vector2f translateWindowCoordinatesToGameCoordinates(
		sf::Vector2f windowCoordinates);

	// indicates if the doctor has requested exit
	bool userRequestedExit = false;

	// stores a pointer to the graphical window
	sf::RenderWindow* window = nullptr;
	sf::Vector2f windowResolution;

	// stores a pointer to the particle manager
	ParticleManager* particleManager = nullptr;

	// stores a pointer to the text manager
	TextManager* textManager = nullptr;
};

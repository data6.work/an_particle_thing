#include <chrono>
#include <thread>
#include <iostream>

#include "SFML/Graphics.hpp"

#include "Settings.h"
#include "TextManager.h"
#include "ParticleManager.h"
#include "InputProcessor.h"

// clears the window according to the current settings
void clearWindow(sf::RenderWindow* window, Settings* settings);

int main()
{
	// particle manager and text manager initializations
	ParticleManager particleManager;
	TextManager textManager;

	// stores the simulation settings
	Settings* settings = particleManager.getSettings();

	// graphical window setup
	sf::VideoMode desktopMode = sf::VideoMode::getDesktopMode();
	sf::RenderWindow window(desktopMode,
							"An_Particle_Thing",
							sf::Style::Fullscreen);

	// input reader initialization
	InputProcessor inputReader(&window, &particleManager, &textManager);

	// stores the targeted 1920x1080y resolution text view
	sf::View textView;
	textView.setSize(1920.f, 1080.f);
	textView.setCenter(1920.f / 2.f, 1080.f / 2.f);

	// stores the simulation-adapted view
	sf::View simulationView;
	simulationView.setSize(particleManager.getSettings()->XLimit,
						   particleManager.getSettings()->YLimit);
	simulationView.setCenter(simulationView.getSize().x / 2,
							 simulationView.getSize().y / 2);
	window.setView(simulationView);


	// this clock allows for time measurement
	sf::Clock clock;

	// main game cycle
	float timeSinceLastFrame = 0.0;
	while (window.isOpen())
	{
		// stores time passed since last cycle
		float timePassed = clock.restart().asSeconds();
		timeSinceLastFrame += timePassed;

		const float desiredFPS = 300.f;
		if (timeSinceLastFrame >= 1.f / desiredFPS)
		{
			// processing of events such as key presses and LMB presses
			inputReader.processInput();

			// clears the window before the next frame
			clearWindow(&window, settings);

			// processes particle movement and draws them
			window.setView(simulationView);
			particleManager.processAndDrawParticles(&window,
				inputReader.getMousePosition(),
				inputReader.isRMBPressed(),
				timeSinceLastFrame);

			// Draws texts on screen.
			// Window view is changed to accomodate different
			// relative drawing coordinates
			window.setView(textView);
			textManager.updateAndDrawTexts(&window,
				settings,
				particleManager.getParticleAmount(),
				timeSinceLastFrame);

			// frame display
			timeSinceLastFrame = 0;
			window.display();
		}
	}

	return 0;
}


void clearWindow(sf::RenderWindow* window, Settings* settings)
{
	// clears window with background settings if clearing is enabled
	if (settings->frameClearingEnabled)
	{
		window->clear(sf::Color((unsigned char)settings->background_red,
								(unsigned char)settings->background_green,
								(unsigned char)settings->background_blue));
	}
}

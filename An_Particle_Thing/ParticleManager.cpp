#include "ParticleManager.h"



ParticleManager::ParticleManager()
{
	// reserves space for particles
	this->particles.reserve(ParticleManager::reserveSize);
	this->bodies.reserve(ParticleManager::reserveSize * 4);
}


void ParticleManager::spawnParticle(sf::Vector2f position, bool spawnMany)
{
	// creates spawn color from settings
	sf::Color spawnColor = { (unsigned char)this->settings.nextColor_red,
							 (unsigned char)this->settings.nextColor_green,
							 (unsigned char)this->settings.nextColor_blue };

	// creates new particle if none particles are on this position
	this->createNewParticle(position, spawnColor);
	
	// creates 4 particles around initial one if requested and no particles
	// are on the same spot
	if (spawnMany)
	{
		if (!this->isAParticleOnThisPosition(position + sf::Vector2f(1.f, 0.f)))
		{
			this->createNewParticle(
				position + sf::Vector2f(1.f, 0.f), spawnColor);
		}
		if (!this->isAParticleOnThisPosition(position + sf::Vector2f(1.f, 1.f)))
		{
			this->createNewParticle(
				position + sf::Vector2f(1.f, 1.f), spawnColor);
		}
		if (!this->isAParticleOnThisPosition(position + sf::Vector2f(-1.f, 0.f)))
		{
			this->createNewParticle(
				position + sf::Vector2f(-1.f, 0.f), spawnColor);
		}
		if (!this->isAParticleOnThisPosition(position + sf::Vector2f(-1.f, -1.f)))
		{
			this->createNewParticle(
				position + sf::Vector2f(-1.f, -1.f), spawnColor);
		}
	}
}


void ParticleManager::processAndDrawParticles(sf::RenderWindow* window,
											  sf::Vector2f attractionPosition,
											  bool attractionEnabled,
											  float timePassed)
{
	// calculates physics time passed
	float physicsTimePassed = this->getPhysicsTimePassed(timePassed);

	// this cycle processes each particle
	int deletionCounter = 0;
	int particleAmount = this->particles.size();
	for (int index = 0; index < particleAmount; ++index)
	{
		// stores the pointer to the current particle
		Particle* particle = &this->particles[index];

		// processes physical parameters if processing is allowed
		if (this->settings.physicsEnabled)
		{
			// changes particle velocity due to attraction
			this->processAttraction(particle,
									physicsTimePassed,
									attractionPosition,
									attractionEnabled);

			// changes particle velocity due to friction
			this->processFriction(&particle->velocity, physicsTimePassed);

			// changes the particle position
			particle->move(physicsTimePassed, &this->bodies[index * 4]);
		}

		// Processes border collisions. Increments counter if a particle is in void
		this->processBorderCollisions(particle, &deletionCounter);
	}

	// deletes everyone up for deletion
	int i = 0;
	while (deletionCounter)
	{
		if (this->particles[i].upForDeletion)
		{
			this->removeParticle(i);
			--i;
			--deletionCounter;
		}
		++i;
	}

	this->drawParticles(window);
}


void ParticleManager::removeAllParticles()
{
	this->particles.clear();
	this->bodies.clear();
}


int ParticleManager::getParticleAmount()
{
	return this->particles.size();
}


void ParticleManager::haltAllMovement()
{
	int particleAmount = this->particles.size();
	for (int index = 0; index < particleAmount; ++index)
	{
		this->particles[index].velocity = { 0.f, 0.f };
	}
}


void ParticleManager::orderParticles()
{
	// this function orders all particles in a horizontal line
	int particleAmount = this->particles.size();
	float currentPositionX = 0.f, currentPositionY = 0.f;
	for (int index = 0; index < particleAmount; ++index)
	{
		// gets the pointer to the current particle
		Particle* particle = &this->particles[index];

		// resets the particle
		particle->velocity = { 0.f, 0.f };
		particle->setPosition({ currentPositionX, currentPositionY });

		// alters next particle position
		currentPositionX += 1.f;

		// checks position validity
		if (currentPositionX > this->settings.XLimit)
		{
			currentPositionX = 0.f;
			currentPositionY += 1.f;
		}
	}
}


Settings* ParticleManager::getSettings()
{
	return &this->settings;
}


void ParticleManager::createNewParticle(sf::Vector2f position, sf::Color color)
{
	if (!this->isAParticleOnThisPosition(position))
	{
		// create a body
		for (int i = 0; i < 4; ++i)
		{
			this->bodies.push_back(sf::Vertex());
		}

		this->particles.push_back(Particle(
			position, color, &this->bodies[this->bodies.size() - 4]));
	}
}


void ParticleManager::processAttraction(Particle* particle,
										float timePassed,
										sf::Vector2f attractionPointPosition,
										bool attractionEnabled)
{
	// returns if attraction is disabled
	if (!attractionEnabled)
	{
		return;
	}

	// finds cordinates difference
	float XDifference = attractionPointPosition.x - particle->getPosition().x;
	float YDifference = attractionPointPosition.y - particle->getPosition().y;

	// alters coordinates if attraction border wrapping is enabled
	if (this->settings.attractionBorderWrapEnabled)
	{
		this->processAttractionWrap(&XDifference, &YDifference);
	}

	// finds the distance
	float distance = sqrt(XDifference * XDifference + YDifference * YDifference);

	// returns if the distance is less than allowed
	if (distance < this->settings.minimumAttractionDistance)
	{
		return;
	}

	// finds absolute velocity change
	float velocityChange =
		timePassed *
		this->settings.attractionPointMass /
		pow(distance, this->settings.attractionDistanceExponent);

	// calculates sine and cosine
	float sine = YDifference / distance;
	float cosine = XDifference / distance;

	// alters particle velocity x and y vectors
	particle->velocity += {velocityChange * cosine,
						   velocityChange * sine};
}


void ParticleManager::processAttractionWrap(float* XDifference,
											float* YDifference)
{
	// process X wrap
	float wrapDistance = this->settings.XLimit - fabsf(*XDifference);
	if (wrapDistance < fabsf(*XDifference))
	{
		*XDifference = -wrapDistance *
			(*XDifference > 0.f ? 1.f : -1.f);
	}

	// process Y wrap
	wrapDistance = this->settings.YLimit - fabsf(*YDifference);
	if (wrapDistance < fabsf(*YDifference))
	{
		*YDifference = -wrapDistance *
			(*YDifference > 0.f ? 1.f : -1.f);
	}

	// uh, originaly the code for each axis was 3 times longer but more
	// readable, but i shortened it out because I didnt know if compiler would
	// optimize it to the maximum
}


void ParticleManager::processFriction(sf::Vector2f* velocity,
									 float timePassed)
{
	// Applies movement friction, depending on dampening option enabled.
	// Different dampening is applied depending on if the time multiplier
	// is positive or negative

	switch (this->settings.unrealisticDampeningEnabled)
	{
	case false:
		*velocity *= (
			1.f - timePassed * this->settings.movementDampeningCoefficient);
		break;

	case true:
		// an incredibly awful old version of dampening
		*velocity /= this->settings.movementDampeningCoefficient *
					 this->settings.timeMultiplier + 1;
		break;
	}
}


void ParticleManager::processBorderCollisions(Particle* particle,
											  int* deletionCounter)
{
	// calls the according function depending on the collision mode
	switch (this->settings.borderMode)
	{
	case 0:
		// wrapping mode
		this->checkWrapBorderCollisons(particle);
		break;

	case 1:
		// reflective mode
		this->checkReflectiveBorderCollisions(particle);
		break;

	case 2:
		// void mode
		// deletes particle if it is out of border and returns true
		if (this->isOutOfBorder(particle->getPosition()))
		{
			particle->upForDeletion = true;
			++*deletionCounter;
		}
	}
}


void ParticleManager::checkWrapBorderCollisons(Particle* particle)
{
	// process X coordinates
	if (particle->getPosition().x > this->settings.XLimit)
	{
		particle->setX(0.f);
		if (this->settings.wrapSpeedPenaltyEnabled)
		{
			particle->velocity *= (this->settings.timeMultiplier > 0.f) ?
				(this->settings.borderCollisionSpeedPenalty) :
				(1.f / this->settings.borderCollisionSpeedPenalty);
		}
	}
	else if (particle->getPosition().x < 0.f)
	{
		particle->setX(this->settings.XLimit);
		if (this->settings.wrapSpeedPenaltyEnabled)
		{
			particle->velocity *= (this->settings.timeMultiplier > 0.f) ?
				(this->settings.borderCollisionSpeedPenalty) :
				(1.f / this->settings.borderCollisionSpeedPenalty);
		}
	}

	// processes Y coordinates
	if (particle->getPosition().y > this->settings.YLimit)
	{
		particle->setY(0.f);
		if (this->settings.wrapSpeedPenaltyEnabled)
		{
			particle->velocity *= (this->settings.timeMultiplier > 0.f) ?
				(this->settings.borderCollisionSpeedPenalty) :
				(1.f / this->settings.borderCollisionSpeedPenalty);
		}
	}
	else if (particle->getPosition().y < 0.f)
	{
		particle->setY(this->settings.YLimit);
		if (this->settings.wrapSpeedPenaltyEnabled)
		{
			particle->velocity *= (this->settings.timeMultiplier > 0.f) ?
				(this->settings.borderCollisionSpeedPenalty) :
				(1.f / this->settings.borderCollisionSpeedPenalty);
		}
	}
}


void ParticleManager::checkReflectiveBorderCollisions(Particle* particle)
{
	// process X coordinates
	if (particle->getPosition().x > this->settings.XLimit)
	{
		particle->setX(2.f * this->settings.XLimit -
							   particle->getPosition().x);

		particle->velocity.x *= (this->settings.timeMultiplier > 0.f) ?
			(-this->settings.borderCollisionSpeedPenalty) :
			(-1.f / this->settings.borderCollisionSpeedPenalty);
	}
	else if (particle->getPosition().x < 0.f)
	{
		particle->setX(-particle->getPosition().x);

		particle->velocity.x *= (this->settings.timeMultiplier > 0.f) ?
			(-this->settings.borderCollisionSpeedPenalty) :
			(-1.f / this->settings.borderCollisionSpeedPenalty);
	}

	// processes Y coordinates
	if (particle->getPosition().y > this->settings.YLimit)
	{
		particle->setY(2.f * this->settings.YLimit - particle->getPosition().y);

		particle->velocity.y *= (this->settings.timeMultiplier > 0.f) ?
			(-this->settings.borderCollisionSpeedPenalty) :
			(-1.f / this->settings.borderCollisionSpeedPenalty);
	}
	else if (particle->getPosition().y < 0.f)
	{
		particle->setY(-particle->getPosition().y);

		particle->velocity.y *= (this->settings.timeMultiplier > 0.f) ?
			(-this->settings.borderCollisionSpeedPenalty) :
			(-1.f / this->settings.borderCollisionSpeedPenalty);
	}
}


bool ParticleManager::isOutOfBorder(sf::Vector2f position)
{
	// return true if the particle is collides/is out of the border
	if (position.x <= 0.f ||
		position.x >= this->settings.XLimit ||
		position.y <= 0.f ||
		position.y >= this->settings.YLimit)
	{
		return true;
	}
	return false;
}


void ParticleManager::drawParticles(sf::RenderWindow* window)
{
	if (this->bodies.size())
	{
		window->draw(
			&this->bodies[0],
			this->bodies.size(),
			this->settings.pointsOrLines ? sf::LineStrip : sf::Quads);
	}
}


float ParticleManager::getPhysicsTimePassed(float actualTimePassed)
{
	// gets effective time passed by applying time multiplier
	float effectiveTimePassed = actualTimePassed *
								this->settings.timeMultiplier;

	// Reduces effective time passed if it is too large.
	// Also signals physics time overdue if needed
	this->settings.maximalPhysicsTimeOverdue = false;
	if (effectiveTimePassed > this->settings.maximalPhysicsTimePassed)
	{
		effectiveTimePassed = this->settings.maximalPhysicsTimePassed;
		this->settings.maximalPhysicsTimeOverdue = true;
	}

	return effectiveTimePassed;
}


bool ParticleManager::isAParticleOnThisPosition(sf::Vector2f position)
{
	// checks all particle positions, returns true if found an
	// identical to the passed argument
	int particleAmount = this->particles.size();
	for (int index = 0; index < particleAmount; ++index)
	{
		if (this->particles[index].getPosition() == position)
		{
			return true;
		}
	}

	return false;
}


void ParticleManager::removeParticle(int index)
{
	this->particles.erase(this->particles.begin() + index);
	this->bodies.erase(this->bodies.begin() + index * 4, this->bodies.begin() + index * 4 + 4);
}

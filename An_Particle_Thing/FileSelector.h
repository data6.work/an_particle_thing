#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <thread>

#include "Settings.h"

// class for selecting files, which operation to perform on them,
// and passing these files onto settings object to save or load settings
class FileSelector
{
public:
	// asks user whether they want to read or write settings to file,
	// calls other functions to process the chosen action
	static void requestSettingsReadWrite(Settings* settings);

private:
	static void readFromFile(Settings* settings);
	static void writeToFile(Settings* settings);

	// returns true if the file with the selected name exists
	static bool doesFileExist(std::string& filename);
};

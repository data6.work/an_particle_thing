#pragma once

#include <string>

#include "SFML/Graphics.hpp"

#include "TextNames.h"

// A custom text class inheriting from SFML text.
// Allows for storing visibility status
class Text : public sf::Text
{
public:
	// Constructor for setting visibility and content.
	// Also applies settings
	Text(TextNames name,
		 bool isVisible = true,
		 std::string content = "");

	bool isVisible;
	TextNames name;
};

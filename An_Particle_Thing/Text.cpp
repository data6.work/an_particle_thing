#include "Text.h"



Text::Text(TextNames name,
		   bool isVisible,
		   std::string content) :
		   isVisible(isVisible),
		   name(name)
{
	this->setString(content);
}

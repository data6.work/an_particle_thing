#include "FileSelector.h"


void FileSelector::requestSettingsReadWrite(Settings* settings)
{
	// initial prompt
	std::cout << "\nInput the number of the operation you want to perform:\n";
	std::cout << "1) Read from settings file\n";
	std::cout << "2) Write to settings file\n";
	std::cout << "Input anything else to go back\n";
	
	// requests input
	std::string input = "";
	std::cin >> input;

	// converts inputed character to an option with the same number
	int option = input[0] - '1' + 1;

	// choses appropriate action depending on the input
	switch (option)
	{
	case 1:
		// read
		FileSelector::readFromFile(settings);
		break;

	case 2:
		// write
		FileSelector::writeToFile(settings);
		break;

	default:
		// return if no option was selected
		return;
	}
}


void FileSelector::readFromFile(Settings* settings)
{
	std::cout << "\nEnter the name of the file to read. Enter nothing to return\n";

	// requests filename until user enters an existing file name
	std::string filename;
	std::getline(std::cin, filename); // it doesent work without this line
	while (true)
	{
		// gets the input string
		std::getline(std::cin, filename);

		if (filename == "")
		{
			// exits on null string
			return;
		}
		else if (FileSelector::doesFileExist(filename))
		{
			// continues if the selected file exists
			break;
		}
		else
		{
			// displays message about file not existing, requests input again
			std::cout << "File does not exist. Enter the name again\n";
		}
	}

	// Sends the chosen filename to the setting's loadFromFile function.
	// Informs about an error on failure
	if (!settings->loadFromFile(filename))
	{
		std::cout << "Error after loading settings from file...\n";
		while (true);
	}
	else
	{
		// informs about success and return to simulation after 1.5 seconds
		std::cout << "Done!\n\n";
		std::this_thread::sleep_for(std::chrono::milliseconds(1500));
	}
}


void FileSelector::writeToFile(Settings* settings)
{
	std::cout << "\nEnter the name of the file to write. ";
	std::cout << "Enter nothing to return\n";

	// requests filename until user enters a valid name
	std::string filename;
	std::getline(std::cin, filename); // doesent work without this line
	while (true)
	{
		// gets the input string
		std::getline(std::cin, filename);

		// returns if null string is entered
		if (filename == "")
		{
			return;
		}

		// asks the user if they want to overwrite an existing file,
		// if there is one
		if (FileSelector::doesFileExist(filename))
		{
			std::cout << "Such file already exists. Overwrite? (1 - Yes)\n";
			int input = 0;
			std::cin >> input;

			// request filename again if overwrite is denied
			if (input != 1)
			{
				std::cout << "Enter the name of the file to write\n";
				continue;
			}
		}

		// leaves the cycle and proceeds to saving settings to file
		break;
	}

	// saves settings to file, informs about at error in case of error
	if (!settings->saveToFile(filename))
	{
		std::cout << "Error after loading settings from file...\n";
		while (true);
	}
	else
	{
		// informs about success and return to simulation after 1.5 seconds
		std::cout << "Done!\n\n";
		std::this_thread::sleep_for(std::chrono::milliseconds(1500));
	}
}


bool FileSelector::doesFileExist(std::string& filename)
{
	std::ifstream fileStream(filename);
	bool result = (bool)fileStream;
	fileStream.close();
	return result;
}

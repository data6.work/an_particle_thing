#pragma once

#include "SFML/Graphics.hpp"

// particle class representing each particle on the screen
class Particle
{
public:
	// spawns particle with the specified position and color
	Particle(sf::Vector2f spawnPosition, sf::Color color, sf::Vertex* vertices);

	void setPosition(sf::Vector2f position);
	void setX(float x);
	void setY(float y);
	sf::Vector2f getPosition();

	// moves the particle based on the time passed
	void move(float timePassed, sf::Vertex* vertices);

	// stores the particle velocity in points/second
	sf::Vector2f velocity = { 0, 0 };

	bool upForDeletion = false;

private:
	sf::Vector2f position = { 0, 0 };
	float offset = 1.5;
};

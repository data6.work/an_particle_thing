#pragma once

// embrace the enum spaghetti

// stores enum names for texts
enum TextNames
{
	// upper left text names
	particleCounter,
	fpsCounter,

	// lower left text names
	physicsEnabled,
	attractionPointMass,
	timeMultiplier,
	attractionDistanceExponent,
	minimumAttractionDistance,
	maximalPhysicsTimePassed,
	movementDampeningCoefficient,
	unrealisticDampeningEnabled,

	// lower right text names
	borderCollisionSpeedPenalty,
	wrapBordersEnabled,
	wrapSpeedPenaltyEnabled,
	attractionBorderWrapEnabled,

	// upper right text names
	controlDescriptionPrompt,

	// control description text names
	controlDescription_0,
	controlDescription_1,
	controlDescription_2,
	controlDescription_3,
	controlDescription_4,
	controlDescription_5,
	controlDescription_6,
	controlDescription_7,
	controlDescription_8,

	//
	// dont use enums bellow for manual text naming
	//

	textAmount,
	firstTextID = TextNames::particleCounter,
	lastTextID = TextNames::textAmount - 1,

	controlDescriptionFirstID = TextNames::controlDescription_0,
	controlDescriptionLastID = TextNames::controlDescription_8,

	mainTextFirstID = TextNames::particleCounter,
	mainTextLastID = TextNames::controlDescriptionPrompt,

	upperLeftFirstID = TextNames::particleCounter,
	upperLeftLastID = TextNames::fpsCounter,

	lowerLeftFirstID = TextNames::physicsEnabled,
	lowerLeftLastID = TextNames::unrealisticDampeningEnabled,

	lowerRightFirstID = TextNames::borderCollisionSpeedPenalty,
	lowerRightLastID = TextNames::attractionBorderWrapEnabled,

	upperRightFirstID = TextNames::controlDescriptionPrompt,
	upperRightLastID = TextNames::controlDescriptionPrompt
};

#pragma once

#include <vector>
#include <string>
#include <iostream>

#include "SFML/Graphics.hpp"

#include "Text.h"
#include "Settings.h"
#include "TextNames.h"

/////////////////////////////////////////////////////
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //
// WARNING: SPAGHETTI CODE WHEN WORKING WITH TEXTS //
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! //
/////////////////////////////////////////////////////

// Stores, updates and draws the texts
class TextManager
{
public:
	// loads font and initializes texts on manager creation
	TextManager();

	// sets up text positions
	void setupTextsPositions();

	// sets up strings for unchangeable texts
	void setUpStaticStrings();

	// updates dynamic texts and draws all texts
	void updateAndDrawTexts(sf::RenderWindow* renderTarget,
							Settings* settings,
							int particleAmount,
							float timePasedSeconds);

	// text settings
	const unsigned characterSize = 30;
	const float outlineSizeToCharacterSize = 0.1f;
	const sf::Color bodyColor = sf::Color::White;
	const sf::Color outlineColor = sf::Color::Black;

	// stores texts
	std::vector<Text> texts;

private:
	// updates text showing simulation parameters
	void updateSettingsTexts(Settings* settings,
							 Text* updateTarget);

	// updates texts which do not rely on simulation settigns
	void updateTextsUnrelatedToSettings(int particleAmount,
										float timePassedSeconds);


	// initializes texts, pushes them onto text vector
	// for further drawing
	void initializeTexts();

	// Applies default settings to the text.
	// (Settings parameters are stored in there)
	void applyTextSettings(Text* targetText);

	// stores text font
	sf::Font textFont;
};
